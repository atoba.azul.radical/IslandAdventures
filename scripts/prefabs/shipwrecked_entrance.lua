local assets=
{
	Asset("ANIM", "anim/portal_shipwrecked.zip"),
	Asset("ANIM", "anim/portal_shipwrecked_build.zip"),
}

local function onhammered(inst, worker)
	inst.components.lootdropper:DropLoot()
	SpawnPrefab("collapse_small").Transform:SetPosition(inst.Transform:GetWorldPosition())
	inst.SoundEmitter:PlaySound("dontstarve/common/destroy_wood")
	inst:Remove()
end

local function onhit(inst, worker)
	inst.AnimState:PlayAnimation("place")
	inst.AnimState:SetTime(0.65 * inst.AnimState:GetCurrentAnimationLength())
	inst.AnimState:PushAnimation("idle_off")
end

local function onbuilt(inst)
	inst.AnimState:PlayAnimation("place")
	inst.AnimState:PushAnimation("idle_off")
	inst.SoundEmitter:PlaySound("ia/common/portal/place")
end

local function OnActivate(inst, doer)
	local paras = {player = doer, portalid = 998, worldid = TheWorld:HasTag("island") and IA_CONFIG.forestid or IA_CONFIG.shipwreckedid}

	inst.components.workable:SetWorkable(false)
	doer.player_classified.ishudvisible:set(false)
	doer:PushEvent("shipwrecked_portal")

	inst:DoTaskInTime(7.5, function()
		TheWorld:PushEvent("ms_playerdespawnandmigrate", paras)
	end)

	inst:DoTaskInTime(8, function(inst)
		inst:Show()
		ChangeToObstaclePhysics(inst)
		inst.components.activatable.inactive = true
		inst.components.workable:SetWorkable(true)
	end)
end

local function SetDestinationWorld(self, world)
	world = nil

	--ensure it link forest or ia world  -Jerry
	if TheWorld:HasTag("island") then
		world = IA_CONFIG.forestid
	else
		world = IA_CONFIG.shipwreckedid
	end

	if world then
		self.inst:PushEvent("open")
	else
		self.inst:PushEvent("close")
	end

	return world
end

local function Open(inst)
    inst.AnimState:PushAnimation("idle_off")

	inst.components.workable:SetWorkable(true)

	inst:AddComponent("activatable")
	inst.components.activatable.OnActivate = OnActivate
    inst.components.activatable.inactive = true
	inst.components.activatable.quickaction = true
end

local function Close(inst)
	inst.AnimState:PlayAnimation("idle_broken")

	inst.components.workable:SetWorkable(false)

	inst:RemoveComponent("activatable")
end

local function fn()
	local inst = CreateEntity()
	inst.entity:AddTransform()
	inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
	inst.entity:AddNetwork()

    inst.entity:AddMiniMapEntity()
	inst.MiniMapEntity:SetIcon("shipwrecked_exit.tex")

	MakeObstaclePhysics(inst, 1)

    inst.AnimState:SetBank("boatportal")
    inst.AnimState:SetBuild("portal_shipwrecked_build")
	inst.AnimState:PlayAnimation("place")
	inst.AnimState:PushAnimation("idle_off")

    inst:AddTag("shipwrecked_portal")

	inst.no_wet_prefix = true

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

	inst:AddComponent("inspectable")

	inst:AddComponent("lootdropper")
	inst:AddComponent("workable")
	inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
	inst.components.workable:SetWorkLeft(4)
	inst.components.workable:SetOnFinishCallback(onhammered)
	inst.components.workable:SetOnWorkCallback(onhit)

	if not TheShard:GetDefaultShardEnabled() then
		Close(inst)
	end

	inst:ListenForEvent("onbuilt", onbuilt)

	inst:AddComponent("worldmigrator")
	inst.components.worldmigrator.id = 998
	inst.components.worldmigrator.receivedPortal = 998
	inst.components.worldmigrator:SetEnabled(false)
	local old_SetDestinationWorld = inst.components.worldmigrator.SetDestinationWorld
    inst.components.worldmigrator.SetDestinationWorld = function(self, world, permanent)
		world = SetDestinationWorld(self, world)
		old_SetDestinationWorld(self, world, permanent)
    end

	inst:ListenForEvent("open", Open)
	inst:ListenForEvent("close", Close)

    return inst
end


SetSharedLootTable('shipwrecked_exit',
{
    {'sunken_boat_trinket_4', 1},
    {'nightmarefuel', 1},
	{'nightmarefuel', 1},
	{'livinglog', 1},
	{'livinglog', 1},
})

-- backwards compatability
local function exit_fn()
	local inst = fn()

    if not TheWorld.ismastersim then
        return inst
    end

	inst.components.lootdropper:SetChanceLootTable('shipwrecked_exit')

	-- for the moment these two prefabs are identical, but leave them as separate prefabs in case
	-- the behaviour ever changes between SW and Forest
	return inst
end

return Prefab( "common/shipwrecked_entrance", fn, assets),
	MakePlacer("shipwrecked_entrance_placer", "boatportal", "portal_shipwrecked_build", "idle_off"),
	Prefab( "common/shipwrecked_exit", exit_fn, assets)