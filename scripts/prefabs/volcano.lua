local assets =
{
	Asset("ANIM", "anim/volcano.zip"),
}

local function OnSummer(inst, issummer, instant)
    if issummer then
        inst.sg:GoToState(instant and "active" or "dormant_pst")
    else
        inst.sg:GoToState(instant and "dormant" or "active_pst")
    end
end

local function OnActivate(inst, doer)
    if doer ~= nil and doer.components.sailor ~= nil then
        if not doer:HasTag("playerghost") then
            local sailor = doer.components.sailor
            if sailor:GetBoat() ~= nil then
                local boat = sailor:GetBoat()
                sailor:Disembark()
                inst:DoTaskInTime(0.5, function()
                    if inst.savedboats[doer.userid] ~= nil then
                        SpawnSaveRecord(inst.savedboats[doer.userid])
                    end
                    inst.savedboats[doer.userid] = boat:GetSaveRecord()
                    boat:Remove()
                end)
            end
        end
    end
end

local function OnActivatedByOther(inst, doer)
    if doer ~= nil then
        if not doer:HasTag("playerghost") then
            local boat = nil
            if inst.savedboats[doer.userid] then
                boat = SpawnSaveRecord(inst.savedboats[doer.userid])
            else
                boat = SpawnPrefab("boat_lograft")
            end
            if boat ~= nil and boat:IsValid() then
                if doer.components.sailor ~= nil then
                    local sailor = doer.components.sailor
                    sailor:Embark(boat)
                end
                inst.savedboats[doer.userid] = nil
            end
        end
    end
end

local function OnWake(inst)
    inst.SoundEmitter:PlaySound("ia/common/volcano/volcano_external_amb", "volcano")
    local state = 1.0
    if inst.sg and inst.sg.currentstate == "dormant" then
        state = 0.0
    end
    inst.SoundEmitter:SetParameter("volcano", "volcano_state", state)
end

local function OnSleep(inst)
    inst.SoundEmitter:KillSound("volcano")
end

-- local maxmod = 70
-- local distToFinish = 10 * 10 --Distance to volcano where you reach max zoom
-- local distToStart = 65 * 65 --Distance from the volcano where you start to zoom

local function CalcCameraDistMod(camera, mod, data)
	local dist = data.inst:GetDistanceSqToPoint(camera.currentpos)
	-- if dist < distToStart then --is in range
	if dist < 4225 then
		mod = mod +
			-- (  dist < distToFinish and maxmod --peak
			(  dist < 100 and 70
			-- or maxmod * (1 - (dist - distToFinish) / (distToStart - distToFinish))  ) --Lerp
			or 70 * (1 - (dist - 100) / 4125)  )
	end
	return mod
end

local function OnRemoveEntity_camera(inst)
	if TheCamera then
		for k, v in pairs(TheCamera.envdistancemods) do
			if v.inst == inst then
				table.remove(TheCamera.envdistancemods, k)
				return
			end
		end
	end
end

local function OnSave(inst, data)
    data.savedboats = inst.savedboats
end

local function OnLoad(inst, data)
    if data and data.savedboats then
        inst.savedboats = data.savedboats
    end
end

local function fn()
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()

	inst.entity:AddAnimState()
    inst.AnimState:SetBuild("volcano")
    inst.AnimState:SetBank("volcano")
    inst.AnimState:PlayAnimation("dormant_idle", true)

	--use "globalmapiconunderfog" prefab to avoid issue #188 ?
    local minimap = inst.entity:AddMiniMapEntity()
    minimap:SetIcon("volcano.tex")

    inst.entity:AddLight()
    inst.Light:SetFalloff(0.4)
    inst.Light:SetIntensity(.7)
    inst.Light:SetRadius(10)
    inst.Light:SetColour(249/255, 130/255, 117/255)
    inst.Light:Enable(true)

    inst:AddTag("blocker")
    inst.entity:AddPhysics()
 	inst.Physics:SetMass(0)
    inst.Physics:SetCollisionGroup(COLLISION.OBSTACLES)
    inst.Physics:ClearCollisionMask()
    inst.Physics:CollidesWith(COLLISION.ITEMS)
    inst.Physics:CollidesWith(COLLISION.CHARACTERS)
	inst.Physics:CollidesWith(COLLISION.WAVES)
    inst.Physics:CollidesWith(COLLISION.OBSTACLES)
    inst.Physics:SetCapsule(10, 5)
    
    inst:AddComponent("waterphysics")
    inst.components.waterphysics.restitution = 1
	--MakeObstaclePhysics(inst, 10)
    inst.entity:AddSoundEmitter()
	inst.entity:AddNetwork()

	inst:AddTag("theVolcano") -- for rainbow jellyfish

    inst.entity:SetPristine()

	if not TheNet:IsDedicated() then
		if TheCamera and TheCamera.envdistancemods then
			table.insert(TheCamera.envdistancemods, {fn = CalcCameraDistMod, inst = inst})
			inst.OnRemoveEntity = OnRemoveEntity_camera
		else
			print(inst,"PANIC! no camera!")
		end
	end

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inspectable")

    inst:AddComponent("waveobstacle")

	--TODO let players climb the volcano
    inst:AddComponent("worldmigrator")   --传送世界
	inst.components.worldmigrator.id = 999
	inst.components.worldmigrator.receivedPortal = 999

    local old_SetDestinationWorld = inst.components.worldmigrator.SetDestinationWorld
    inst.components.worldmigrator.SetDestinationWorld = function(self, world, permanent)
        world = IA_CONFIG.volcanoid or world  --Guaranteed in multilayer world to teleport players to volcan world  -Jerry
        old_SetDestinationWorld(self, world, permanent)
    end

	local old_Activate = inst.components.worldmigrator.Activate --Overide Activate function
    inst.components.worldmigrator.Activate = function(self, doer)
		OnActivate(inst, doer)
		return old_Activate(self, doer)
	end

	local old_ActivatedByOther = inst.components.worldmigrator.ActivatedByOther --Overide ActivatedByOther function
	inst.components.worldmigrator.ActivatedByOther = (function(self)
		local doer = LastMigrator
		OnActivatedByOther(inst, doer)
		old_ActivatedByOther(self)
	end)


    inst.OnLoadPostPass = function(inst, ents, data)
    	TheWorld.components.volcanomanager:AddVolcano(inst)
  	end

    inst.OnRemoveEntity = function(inst)
    	TheWorld.components.volcanomanager:RemoveVolcano(inst)
    end

	--TODO handle those elsehow (worldstate probably won't work)
    inst:ListenForEvent("OnVolcanoEruptionBegin", function (it)
        if inst and inst.sg then
            inst.sg:GoToState("erupt")
        end
       -- -- print(">>>OnVolcanoEruptionBegin", inst)
    end, TheWorld)

    inst:ListenForEvent("OnVolcanoEruptionEnd", function (it)
       if inst and inst.sg then
            inst.sg:GoToState("rumble")
        end
       -- -- print(">>>OnVolcanoEruptionEnd", inst)
    end, TheWorld)

    inst:ListenForEvent("OnVolcanoWarningQuake", function (it)
       if inst and inst.sg then
           inst.sg:GoToState("rumble")
        end
       -- -- print(">>>OnVolcanoEruptionEnd", inst)
    end, TheWorld)
    inst:SetStateGraph("SGvolcano")

    OnSummer(inst, TheWorld.state.issummer, true)
    inst:WatchWorldState("issummer", OnSummer)

    inst.OnEntityWake = OnWake
    inst.OnEntitySleep = OnSleep

    inst.savedboats = {}
    inst.OnSave = OnSave
    inst.OnLoad = OnLoad

	return inst
end

return Prefab("volcano", fn, assets)
