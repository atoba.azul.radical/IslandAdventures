-- This speech is for Wortox
return {

	ACTIONFAIL =
	{
		REPAIRBOAT = 
		{
			GENERIC = "Oh I simply couldn't.",
		},
		EMBARK = 
		{
			INUSE = "I could hop over, I suppose.",
		},
		INSPECTBOAT = 
		{
			INUSE = GLOBAL.STRINGS.CHARACTERS.GENERIC.ACTIONFAIL.STORE.INUSE
		},
		OPEN_CRAFTING  = 
   		{
			FLOODED = "With much regret, it's far too wet.",
		}, 
	},
	
	ANNOUNCE_MAGIC_FAIL = "There's a time and place for everything.",
	
	ANNOUNCE_SHARX = "Hyuyu... Whoopsie.",
	
	ANNOUNCE_TREASURE = "Ohoho, what have we here?",
	ANNOUNCE_MORETREASURE = "We're on a roll, hyuyu!",
	ANNOUNCE_OTHER_WORLD_TREASURE = "Not from this world or the one below!",
	ANNOUNCE_OTHER_WORLD_PLANT = "It doesn't fit in here.",
	
	ANNOUNCE_IA_MESSAGEBOTTLE =
	{
		"The message is long gone. And that's done.",
	},
	ANNOUNCE_VOLCANO_ERUPT = "Oh dear, fire and brimstone abound!",
	ANNOUNCE_MAPWRAP_WARN = "I'm quite opti-mist-ic this leads somewhere!",
	ANNOUNCE_MAPWRAP_LOSECONTROL = "I haven't the foggiest where this leads!",
	ANNOUNCE_MAPWRAP_RETURN = "I tried to catch the fog... but I mist! Hyuyuyu!",
	ANNOUNCE_CRAB_ESCAPE = "A slippery soul! I've lost my goal.",
	ANNOUNCE_TRAWL_FULL = "Hyuyu, that was reel-y fun!",
	ANNOUNCE_BOAT_DAMAGED = "Oh dear, my boat's quite the drear.",
	ANNOUNCE_BOAT_SINKING = "I'm up to my tail in water!",
	ANNOUNCE_BOAT_SINKING_IMMINENT = "Until land is near, I must persevere!",
	ANNOUNCE_WAVE_BOOST = "Hyuyuyu!",
	
	ANNOUNCE_WHALE_HUNT_BEAST_NEARBY = "Bubbles, bubbles.",
	ANNOUNCE_WHALE_HUNT_LOST_TRAIL = "You win this once, whale!",
	ANNOUNCE_WHALE_HUNT_LOST_TRAIL_SPRING = "Sabotaged by this realm's mushiness.",
	
	DESCRIBE = {
	
		GHOST_SAILOR = "A swashbuckling, seafaring soul!",
		FLOTSAM = "What a sad end.",
		SUNKEN_BOAT = 
		{
			GENERIC = "A talking bird partner! Think of all the pranks!",
			ABANDONED = "So long, my feathered friend.",
		},
		SUNKEN_BOAT_BURNT = "That's just mean.",

		BOAT_LOGRAFT = "It's not so strong, but I don't have long.",
		BOAT_RAFT = "It's an improvement, I suppose.",
		BOAT_ROW = "Gently down the merry stream!",
		BOAT_CARGO = "So that my vessel may bear my burdens.",
		BOAT_ARMOURED = "Delightfully decorated!",
		BOAT_ENCRUSTED = "My vessel has gone stone cold!",
		CAPTAINHAT = "Hyuyu, call me captain!",

		BOAT_TORCH = "A light at sea, that I may see.",
		BOAT_LANTERN = "Guide my way among the waves!",
		BOATREPAIRKIT = "No soggy imp mishaps on this fine day!",
		BOATCANNON = "This is going to be so much fun to use!",

		BOTTLELANTERN = "Little jittery, glittery souls inside.",
		BIOLUMINESCENCE = "How kind of you to guide our way.",

		BALLPHIN = "What a nasty trickster!",
		BALLPHINHOUSE = "The tricksters build with their fins, I suppose.",
		DORSALFIN = "For a nice afternoon of terrorizing mortals.",
		TUNACAN = "A fishy surprise awaits inside!",

		JELLYFISH = "Now to sting some unsuspecting mortal.",
		JELLYFISH_DEAD = "Good night, seaborne prankster.",
		JELLYFISH_COOKED = "Is this supposed to be a mortal delicacy?",
		JELLYFISH_PLANTED = "Pranksters of the sea!",
		JELLYJERKY = "Looks chewy. I hate chewing.",
		RAINBOWJELLYFISH = "I'll try not to suck you dry.",
		RAINBOWJELLYFISH_PLANTED = "So colorful and bright, it's such a sight!",
		RAINBOWJELLYFISH_DEAD = "A sad end, my colorful friend.",
		RAINBOWJELLYFISH_COOKED = "I guess these ones do look kind of good.",
		RAINBOWJELLYJERKY = "I can't decide if that's pretty or mortifying.",
		JELLYOPOP = "Mortals innovate when they're deprived of snacks.",

		CROCODOG = "I'd steer clear of those nasty chompers!",
		POISONCROCODOG = "Yikes! It packs sick bites!",
		WATERCROCODOG = "Ugh, you smell of soggy imp!",
	
		PURPLE_GROUPER = "What a silly-looking fish.",
		PIERROT_FISH = "Time to land on a plate, little soul.",
		NEON_QUATTRO = "A fish of blue, to land in a stew.",
		PURPLE_GROUPER_COOKED = "It's beheaded now.",
		PIERROT_FISH_COOKED = "It's not exactly my taste.",
		NEON_QUATTRO_COOKED = "In fire it's rolled, and yet it's cold.",
		TROPICALBOUILLABAISSE = "A fairly potent fish brew.",

		FISH_FARM = 
		{
			EMPTY = "Farming souls sounds just a bit cruel.",
			STOCKED = "Good night, my dears.",
			ONEFISH = "There's a fish! Just as I wished!",
			TWOFISH = "For company it longed, so it brought a friend along.",
			REDFISH = "There's a third, they've formed a herd!",
			BLUEFISH  = "So many souls! I'll hate to part with them.",
		},
	
		ROE = "There's many little fishies hiding, inside.",
		ROE_COOKED = "The souls have been cooked out of these poor children.",
		CAVIAR = "A fancy mortal meal.",

		CORMORANT = "It's the fancy stork that ferries the fish infants.",
		SEAGULL = "What a nasty, greedy bird.",
		SEAGULL_WATER = "Begone you greedy, greedy bird.",
		TOUCAN = "My, what a silly beak!",
		PARROT = "We could be good friends, you and I.",
		PARROT_PIRATE = "Ooh, how dashing!",

		SEA_YARD =
		{
			ON = "It toils so I may rest my impy back.",
			OFF = "Flick that switch, it's quite the fix!",
			LOWFUEL = "Its tummy aches for some tar.",
		},
	
		SEA_CHIMINEA = 
		{
			EMBERS = "Soon to extinguish.",
			GENERIC = "It's a flame afloat.",
			HIGH = "Those are some spicy flames.",
			LOW = "It burns so low, so low!",
			NORMAL = "It burns safe from the vicious winds.",
			OUT = "Cinders, cinders, cinders.",
		}, 

		CHIMINEA = "So that I may warm up amid the cruel storms.",

		TAR_EXTRACTOR =
		{
			ON = "Much better than getting it on my fur.",
			OFF = "It is ready to extract at a moment's notice.",
			LOWFUEL = "It could use some of its own produce, if you ask me.",
		},

		TAR = "It smells and feels horrible.",
		TAR_TRAP = "They won't be so quick with this goopy little trick.",
		TAR_POOL = "It's an oceanic soup of black goop.",
		TARLAMP = "Better to bear the smell than the brunt of darkness.",
		TARSUIT = "Perhaps I'd rather be a soggy imp for the day.",

		PIRATIHATITATOR =
		{
			GENERIC = "It pillages the magical knowledge!",
			BURNT = "That's one way to neutralize pirate magic.",
		},

		PIRATEHAT = "Do I make a dashing pirate?",

		MUSSEL_FARM =
		{
			GENERIC = "There are many shellfish, down there, in the deep.",
			STICKPLANTED = "The mussels crave those bamboo."
		},

		MUSSEL = "What strange critters. Where are their souls?",
		MUSSEL_COOKED = "A fancy meal for mortals and imps alike.",
		MUSSELBOUILLABAISE = "This mortal broth is nice and stomachable, indeed.",
		MUSSEL_BED = "Time to get to bed, little ones. Hyuyu!",
		MUSSEL_STICK = "Mussels on a stick will do the trick!",

		LOBSTER = "Careful, don't pinch my hands.",
		LOBSTER_DEAD = "The best part's already gone.",
		LOBSTER_DEAD_COOKED = "This seemed more appealing when the mortals praised it.",
		LOBSTERHOLE = "Even crustaceans need a place to sleep every now and den.",
		SEATRAP = "The lobsters will find this so funny!",

		BUSH_VINE =
		{
			BURNING = "Whoopsadoodle.",
			BURNT = "Well, that was fun.",
			CHOPPED = "Now to weave!",
			GENERIC = "Home of the slithering sirs.",
		},
		VINE = "It's a decent vine, fit for many a design.",
		DUG_BUSH_VINE = "A vine in need of a spine.",
	
		ROCK_LIMPET =
		{
			GENERIC = "Those don't seem like they'd bear souls.",
			PICKED = "Gone, picked clean.",
		},

		LIMPETS = "I hear these are a mortal delicacy.",
		LIMPETS_COOKED = "It's supposedly more flavorful now.",
		BISQUE = "The snails have been goopified!",

		MACHETE = "To chop and slash!",
		GOLDENMACHETE = "Fit for a sophisticated imp!",

		THATCHPACK = "It won't hold much, but might come in crutch.",
		PIRATEPACK = "Is there a nymph slipping in coins every now and then?",
		SEASACK = "Cold as the morning waves!",

		SEAWEED_PLANTED =
        {
            GENERIC = "Is that weed I see in the water?",
            PICKED = "I see no more weed here.",
        },

		SEAWEED = "Perhaps I'll find a hungry mortal to pass this off to.",
		SEAWEED_COOKED = "I would rather have a pile of souls.",
		SEAWEED_DRIED = "I do not want it.",
		SEAWEED_STALK = "So that I may see the weeds wherever I want them.",

		DUBLOON = "Money is soulless.",
		SLOTMACHINE = "Do you have anything for this fortituous imp today?",
		
		SOLOFISH = "Pardon me if I don't give you belly rubs.",
		SOLOFISH_DEAD = "Poor little critter.",
		SWORDFISH = "It's a swimming swashbuckler!",
		SWORDFISH_DEAD = "Looks like it's swashed its last bucks!",
		CUTLASS = "To swash and stab!",

		SUNKEN_BOAT_TRINKET_1 = "What a silly contraption!", --sextant
		SUNKEN_BOAT_TRINKET_2 = "It's a toy for little mortals.", --toy boat
		SUNKEN_BOAT_TRINKET_3 = "I hear these smell pleasant when lit.", --candle
		SUNKEN_BOAT_TRINKET_4 = "So what is the sea worth?", --sea worther
		SUNKEN_BOAT_TRINKET_5 = "Not to toot my own horn, but I seem to be getting better at landing soles!", --boot
		TRINKET_IA_13 = "Not even the mortals want to open this one.", --orange soda
		TRINKET_IA_14 = "Ooohoho, mischief magicks!", --voodoo doll
		TRINKET_IA_15 = "To strum and sing.", --ukulele
		TRINKET_IA_16 = "I have no earthly clue what this is for.", --license plate
		TRINKET_IA_17 = "Not to toot my own horn, but I seem to be getting better at landing soles!", --boot
		TRINKET_IA_18 = "An artifact!", --vase
		TRINKET_IA_19 = "My head's clouded as it is.", --brain cloud pill
		TRINKET_IA_20 = "What a silly contraption!", --sextant
		TRINKET_IA_21 = "It's a toy for little mortals.", --toy boat
		TRINKET_IA_22 = "I hear these smell pleasant when lit.", --wine candle
		TRINKET_IA_23 = "What in the world is this for?", --broken aac device
		EARRING = "The ring! Cast it into the fire!",

		TURF_BEACH = "Floor or ceiling, depending on your perspective.",
		TURF_JUNGLE = "Floor or ceiling, depending on your perspective.",
		TURF_MAGMAFIELD = "Floor or ceiling, depending on your perspective.",
		TURF_TIDALMARSH = "Floor or ceiling, depending on your perspective.",
		TURF_ASH = "Floor or ceiling, depending on your perspective.",
		TURF_MEADOW = "Floor or ceiling, depending on your perspective.",
		TURF_VOLCANO = "Like a small parcel of dear old home.",
		TURF_SWAMP = "Floor or ceiling, depending on your perspective.",
		TURF_SNAKESKIN = "What a stylish red!",

		WHALE_BLUE = "Hyuyu, why be so blue?",
		WHALE_CARCASS_BLUE = "Oh, dear.",
		WHALE_WHITE = "This one's white! We're in for a fight!",
		WHALE_CARCASS_WHITE = "Well, that was fun.",
		WHALE_TRACK = "Bubbles, bubbles.",
		WHALE_BUBBLES = "Do these lead to fresh trouble?",
		BLUBBERSUIT = "Perhaps those belong better in a mortal's stew.",
		BLUBBER = "Quite squishy!",
		HARPOON = "Impaling things feels terrifyingly innate.",

		SAIL_PALMLEAF = "Gentle green glider, that I may go with the breeze.",
		SAIL_CLOTH = "I shall sail forth with this clean cloth!",
		SAIL_SNAKESKIN = "It's red like me!",
		SAIL_FEATHER = "Now the bird can fly, over the open seas!",
		IRONWIND = "It's brimming with wind energy.",

		BERMUDATRIANGLE = "Where does that lead? Oh, how neat!",
	
		PACKIM_FISHBONE = "Good little fish, are you aware you've no soul?",
		PACKIM = "Greetings my hungry, feathered friend.",

		TIGERSHARK = "What manner of horrible creature is that?",
		MYSTERYMEAT = "Goodness gracious, no no no!",
		SHARK_GILLS = "A souvenir, to remember fun times by.",
		TIGEREYE = "I've got it! Apologies, good lady.",
		DOUBLE_UMBRELLAHAT = "Hyuyu, it's so silly!",
		SHARKITTEN = "Forgive me that I won't be petting you.",
		SHARKITTENSPAWNER = 
		{
			GENERIC = "The den of that most fearsome beast.",
			INACTIVE = "It's deserted.",
		},

		WOODLEGS_KEY1 = "Something, somewhere must be locked.",--Unused
		WOODLEGS_KEY2 = "This key probably unlocks something.",--Unused
		WOODLEGS_KEY3 = "That's a key.",--Unused
		WOODLEGS_CAGE = "Someone must've been very naughty to be locked up like that.",--Unused

		CORAL = "How vibrant!",
		ROCK_CORAL = "I've no quarrel with the corals.",
		LIMESTONENUGGET = "I thought it was supposed to be made of lime.",
		NUBBIN = "I'll tell you nothin', nubbin.",
		CORALLARVE = "I surmise it's not at full size.",
		WALL_LIMESTONE = "To keep you out, or to keep me in?",
		WALL_LIMESTONE_ITEM = "It's of no use there on the ground.",
		WALL_ENFORCEDLIMESTONE = "To keep you out, or to keep me in?",
		WALL_ENFORCEDLIMESTONE_ITEM = "It's of no use off the sea.",
		ARMORLIMESTONE = "Stone cold!",
		CORAL_BRAIN_ROCK = "Ooh, it tickles my little impy brain!",
		CORAL_BRAIN = "What a curious thing!",
		BRAINJELLYHAT = "Hyuyu, it fills my little imp head with so much knowledge!",

		SEASHELL = "A seaborne souvenir I shelldom care to sea.",
		SEASHELL_BEACHED = "It's a shell, how swell!",
		ARMORSEASHELL = "I think it brings out my horns.",

		ARMOR_LIFEJACKET = "Drowning would be a nasty way to lose my soul.",
		ARMOR_WINDBREAKER = "Must it come in pink? A red one's nicer, I'd think.",

		SNAKE = "Well 'tssssss' to you too!",
		SNAKE_POISON = "Do not bite me, oh please!",
		SNAKESKIN = "I prefer fur to scales.",
		SNAKEOIL = "Hyuyu, what a silly deception!",
		SNAKESKINHAT = "If only it had horn holes.",
		ARMOR_SNAKESKIN = "To keep my impish fur slick.",
		SNAKEDEN =
		{
			BURNING = "Whoopsadoodle.",
			BURNT = "Well, that was fun.",
			CHOPPED = "Now to weave!",
			GENERIC = "Home of the slithering sirs.",
		},

		OBSIDIANFIREPIT =
		{
			EMBERS = "Soon to extinguish.",
			GENERIC = "Hyuyu, what a pretty pit!",
			HIGH = "Those are some really spicy flames.",
			LOW = "It burns so low, so low!",
			NORMAL = "Quite cozy!",
			OUT = "Cinders, cinders, cinders.",
		},

		OBSIDIAN = "It's red and bright, like me!",
		ROCK_OBSIDIAN = "A mere device simply won't suffice.",
		OBSIDIAN_WORKBENCH = "A lovely spot for a bit of magic tinkering.",
		OBSIDIANAXE = "To chop and set ablaze!",
		OBSIDIANMACHETE = "It was made just for me!",
		SPEAR_OBSIDIAN = "To impale and scorch!",
		VOLCANOSTAFF = "Hyuyu, what devilish magicks!",
		ARMOROBSIDIAN = "To keep my fur burn-free.",
		COCONADE =
		{
			BURNING = "Now's the time, it's do or die!",
			GENERIC = "It's such a nutty trick!",
		},

		OBSIDIANCOCONADE =
		{
			BURNING = "Now's the time, it's do or die!",
			GENERIC = "It's gotten even nuttier with that red glass!",
		},

		VOLCANO_ALTAR =
		{
			GENERIC = "Keep it still, we will, we will.",
			OPEN = "The great volcano demands offerings!",
		},

		VOLCANO = "It reminds me of home, it does.",
		VOLCANO_EXIT = "Back to the mortal plane.",
		ROCK_CHARCOAL = "Pre-burnt boulders.",
		VOLCANO_SHRUB = "Someone played a prank on this tree.",
		LAVAPOOL = "I won't dip my toe in.",
		COFFEEBUSH =
		{
			BARREN = "It craves some ashes and brimstone.",
			WITHERED = "It will need more ashes to grow any more batches.",
			GENERIC = "A fiery bush!",
			PICKED = "Gone, all gone.",
		},

		COFFEEBEANS = "Hmm, they seem to have a bit of magic about them.",
		COFFEEBEANS_COOKED = "Hyuyu, what strange beans!",
		DUG_COFFEEBUSH = "Put it in the ash, before it becomes trash!",
		COFFEE = "This mortal drink is sure to put a pep in one's step, hyuyu!",

		ELEPHANTCACTUS =
		{
			BARREN = "It needs a little ash, before it becomes brash.",
			WITHERED = "It needs a little ash, before it becomes brash.",
			GENERIC = "Hyuyu, what a nasty plant!",
			PICKED = "The greater prankster wins this one.",
		},

		DUG_ELEPHANTCACTUS = "This would devise a nasty surprise.",
		ELEPHANTCACTUS_ACTIVE = "Hyuyu, what a nasty plant!",
		ELEPHANTCACTUS_STUMP = "The greater prankster wins this one.",
		NEEDLESPEAR = "Sharp as can be, as you can see.",
		ARMORCACTUS = "Every good imp knows how to blend in.",
		
		TWISTER = "A spirit of wind!",
		TWISTER_SEAL = "So how did you stir up so much trouble, hm?",
		TURBINE_BLADES = "So this is the source of the heavenly magicks.",
		MAGIC_SEAL = "A treasure, to remember your ferocious gales by.",
		WIND_CONCH = "To blow the winds themselves!",
		WINDSTAFF = "I go wherever the wind takes me. Literally!",

		DRAGOON = "A horrible brute, and a jock to the boot!",
		DRAGOONHEART = "Hyuyu, it's still beating!",
		DRAGOONSPIT = "I won't dip my toe in.",
		DRAGOONEGG = "What's inside is a nasty surprise!",
		DRAGOONDEN = "Home of the brutish sirs.",

		ICEMAKER = 
		{
			OUT = "And out it goes.",
			VERYLOW = "It needs some fuel, if it's to toil!",
			LOW = "It craves something to burn into ice.",
			NORMAL = "Chilly, chilly!",
			HIGH = "It froze over early this year!",
		},

		HAIL_ICE = "It knows how to chill, hyuyu.",
	
		BAMBOOTREE =
		{
			BURNING = "Oopsie.",
			BURNT = "Well, that was fun.",
			CHOPPED = "Let's get a move on. Chop chop!",
			GENERIC = "They grow up so fast!",
		},

		BAMBOO = "What foreign greenery!",
		FABRIC = "To rest my little head upon.",
		DUG_BAMBOOTREE = "Put in in the mud, before it gets hurt!",
		
		JUNGLETREE =
		{
			BURNING = "Twiddle dee dee, a burning tree!",
			BURNT = "It can burn again, I suppose.",
			CHOPPED = "A funny jungle prank.",
			GENERIC = "An enormous tree of stripes and leaves.",
		},

		JUNGLETREESEED = "A teensy little baby tree!",
		JUNGLETREESEED_SAPLING = "Grow big and tall, or not at all.",
		LIVINGJUNGLETREE = "It's as perplexing as it is vexing.",

		OX = "A majestic beast of curved horns.",
		BABYOX = "So little.",--unused
		OX_HORN = "Hmph, mines are better.",
		OXHAT = "Horns to wear over your horns.",
		OX_FLUTE = "I'll sing a song, the world shall weep along!",

		MOSQUITO_POISON = "Keep that mouth to yourself, good sir!",
		MOSQUITOSACK_YELLOW = "What mortal blood is this?",

		STUNGRAY = "You're no fun at all!",
		POISONHOLE = "I wouldn't do that sort of prank.",
		GASHAT = "Keeps my snoot reek-free.",

		ANTIVENOM = "A potent cure for the insecure.",
		VENOMGLAND = "Now to land this gland in a grand brew.",
		POISONBALM = "Hyuyu, someone's been getting crafty!",
		
		SPEAR_POISON = "What a terrible thing! I can't wait to use it.",
		BLOWDART_POISON = "I'd hate to inhale that by mistake.",

		SHARX = "I think nature played a prank on that one.",
		SHARK_FIN = "A severed fin that'll never again swim.",
		SHARKFINSOUP = "It did swim again after all, hyuyu!",
		SHARK_TEETHHAT = "It restles nicely upon my horns.",
		AERODYNAMICHAT = "How silly and how ingenious!",

		IA_MESSAGEBOTTLE = "Ooh, a message!",
		IA_MESSAGEBOTTLEEMPTY = "An empty bottle for bits and bobs.",
		BURIEDTREASURE = "Treasure, how exciting!",

		SAND = "It's just some sand, if you understand.",
		SANDDUNE = "It's a pile of sand.",
		SANDBAGSMALL = "So that the sea does not swallow all!",
		SANDBAGSMALL_ITEM = "Ballast!",
		SANDCASTLE =
		{
			SAND = "Their poor kingdom has collapsed.",
			GENERIC = "Hyuyu, what a lovely little kingdom!"
		},

		SUPERTELESCOPE = "Nothing can hide from an imp, hyuyu!",
		TELESCOPE = "Perfect tool to stalk some mortals with.",
		
		DOYDOY = "I've never met a mortal this stupid.",
		DOYDOYBABY = "Why you silly little thing, you're so cute!",
		DOYDOYEGG = "Hatches one feathered fool.",
		DOYDOYEGG_COOKED = "Poor little dimwit.",
		DOYDOYFEATHER = "Hyuyu, it's enormous!",
		DOYDOYNEST = "A refuge, for the feebleminded.",
		TROPICALFAN = "Many large feathers to brave hot weather.",
	
		PALMTREE =
		{
			BURNING = "Oopsadoodle.",
			BURNT = "Well, that happened.",
			CHOPPED = "A silly tree prank.",
			GENERIC = "To rest my hooves under.",
		},

		COCONUT = "It's nutty, like me!",
		COCONUT_HALVED = "It may be eaten now.",
		COCONUT_COOKED = "I heard it's tastier this way.",
		COCONUT_SAPLING = "Nutty little sapling!",
		PALMLEAF = "Hyuyu, fan me!",
		PALMLEAF_UMBRELLA = "To keep my head fur nice and dry.",
		PALMLEAF_HUT = "Delightful shade!",
		LEIF_PALM = "It is no longer bound to the sands!",

		CRAB = 
		{
			GENERIC = "Crabby little souls.",
			HIDDEN = "A slippery soul, this one.",
		},

		CRABHOLE = "The hole which all the crabbits go!",

		TRAWLNETDROPPED = 
		{
			SOON = "Soon it'll sink into the sea.",
			SOONISH = "The trawl net floats for now.",
			GENERIC = "Let's look at my haul, ere sea swallows all.",
		},

		TRAWLNET = "Ooh, are we hunting for treasure?",
		IA_TRIDENT = "Holding a pitchfork feels disturbingly natural.",

		KRAKEN = "Goodness gracious, you have many arms!",
		KRAKENCHEST = "How sweet, it did leave us a gift!",
		KRAKEN_TENTACLE = "Keep those to yourself, brutish beast.",
		QUACKENBEAK = "To gnaw no more.",
		QUACKENDRILL = "Much drier than digging with paws or claws.",
		QUACKERINGRAM = "Make way, make way for the imp!",

		MAGMAROCK = "What a shock! It's a rock!",
		MAGMAROCK_GOLD = "Glitter, glitter, glitter.",
		FLAMEGEYSER = "Watch that flame! It's not at all tame!",

		TELEPORTATO_SW_RING = "Looks like I could use this.",--unused
		TELEPORTATO_SW_BOX = "It looks like a part for something.",--unused
		TELEPORTATO_SW_CRANK = "I wonder what this is used for.",--unused
		TELEPORTATO_SW_POTATO = "Seems like it was made with a purpose in mind.",--unused
		TELEPORTATO_SW_BASE = "I think it's missing some parts.",--unused
		
		PRIMEAPE = "How do you do, little sir?",
		PRIMEAPEBARREL = "Did the maid take the year off? Hyuyu!",
		MONKEYBALL = "It's so delightfully silly!",
		WILBUR_UNLOCK = "He look like king!",--unused
		WILBUR_CROWN = "It for monkey!",--unused

		MERMFISHER = "Goodness gracious, what devoted fishmongers!",
		MERMHOUSE_FISHER = "Home to the fisherfolks.",

		OCTOPUSKING = "Perhaps we'll earn a favor, if we offer some labor.",
		OCTOPUSCHEST = "What's inside? It's such a ride!",

		SWEET_POTATO = "I do not want it.",
		SWEET_POTATO_COOKED = "It's only slightly better.",
		SWEET_POTATO_PLANTED = "It's a tuber that's in the ground.",
		SWEET_POTATO_SEEDS = "Grow a seed and you shall feed!",
		SWEETPOTATOSOUFFLE = "I'm sure it's great, but it's just not to my taste.",

		BOAT_WOODLEGS = "Does this make one look piratical?",
		WOODLEGSHAT = "This would complement my horns.",
		SAIL_WOODLEGS = "It's a pirate flag, not at all a gag!",

		PEG_LEG = "What a silly thing!",
		PIRATEGHOST = "A seafaring, swashbuckling soul!",

		WILDBORE = "Hyuyu, what a short temper!",
		WILDBOREHEAD = "Yikes.",
		WILDBOREHOUSE = "A house I may blow down.",

		MANGROVETREE = "What do I see? A seaborne tree!",
		MANGROVETREE_BURNT = "How ironic!",

		PORTAL_SHIPWRECKED = "It broken.",--In SW it's used for broken seaworthy --unused
		SHIPWRECKED_ENTRANCE = "Let's skip, hop and jump right into another plane!",
		SHIPWRECKED_EXIT = "Is it time to hop back already?",

		TIDALPOOL = "I could stare at my reflection all day!",
		FISHINHOLE = "Extra fishy!",
		FISH_TROPICAL = "The soul's left this one.",
		TIDAL_PLANT = "A little leafy plant I see.",
		MARSH_PLANT_TROPICAL = "Move, I'm gazing here.",

		FLUP = "I can only appreciate a master of disguise!",
		BLOWDART_FLUP = "Without its eye, it's just a 'fsh'. Fshhh.",

		SEA_LAB = "I have no idea how any of this works!",
		BUOY = "A little mark to light my way.", 
		WATERCHEST = "A floaty space for bits and bobs.",

		LUGGAGECHEST = "It wouldn't hurt to take an itty peek inside.",
		WATERYGRAVE = "It's sad. Full of regrets.",
		SHIPWRECK = "The crew is long gone.",
		BARREL_GUNPOWDER = "That's a dangerous prank!",
		RAWLING = "A soulless partner for a lonely mortal.",
		GRASS_WATER = "Extra-waterlogged!",
		KNIGHTBOAT = "A knight in rust-free armor!",

		DEPLETED_BAMBOOTREE = "Will it grow again?",--unused?
		DEPLETED_BUSH_VINE = "Will it come back?",--unused?
		DEPLETED_GRASS_WATER = "Poor plant, florp.",--unused?

		WALLYINTRO_DEBRIS = "Broken ship.", --unused
		BOOK_METEOR = "She really needs to be more discreet with her knowledge.",
		CRATE = "It's a crate! How great!",
		SPEAR_LAUNCHER = "Shoot, and I'll move!",
		MUTATOR_TROPICAL_SPIDER_WARRIOR = "A tasty treat to those tiny terrors!",
	},
	}
