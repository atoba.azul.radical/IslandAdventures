local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

local LAND = "land"
local WATER = "water"

IAENV.AddComponentPostInit("shadowcreaturespawner", function(cmp)
    local StartSpawn
    for i, v in ipairs(cmp.inst.event_listening["ms_playerjoined"][TheWorld]) do
    	if UpvalueHacker.GetUpvalue(v, "OnInducedInsanity") then --check for this, since Start is a fairly common function name.
            StartSpawn =  UpvalueHacker.GetUpvalue(v, "Start", "UpdatePopulation", "StartSpawn")
    		break
    	end
    end
    if not StartSpawn then return end

    local _UpdateSpawn =  UpvalueHacker.GetUpvalue(StartSpawn, "UpdateSpawn")
    local _StartTracking =  UpvalueHacker.GetUpvalue(_UpdateSpawn, "StartTracking")

    --No need to set this upvalue, it isn't used anywhere but in our UpdateSpawn edit.
    local function StartTracking(player, params, ent, ...)
        ent:ListenForEvent("embarkboat", function()
            if not ent:CanOnWater() then
    			ent.spawnedforplayer = nil
    			ent.persists = false
    			ent.wantstodespawn = true
    		end
        end, player)
        ent:ListenForEvent("disembarkboat", function()
            if not ent:CanOnLand() then
    			ent.spawnedforplayer = nil
    			ent.persists = false
    			ent.wantstodespawn = true
    		end
        end, player)
    	return _StartTracking(player, params, ent, ...)
    end

    local function UpdateSpawn(player, params, ...)
        if params.targetpop > #params.ents then
            local playerpos = player:GetPosition()
            local tile_type = GetVisualTileType(playerpos:Get())
            local land_or_water = IsLand(tile_type) and LAND or IsWater(tile_type) and WATER
            if land_or_water ~= nil then
                local find_offset_fn = land_or_water == LAND and FindGroundOffset or land_or_water == WATER and FindWaterOffset
                local offset = find_offset_fn(playerpos, 2*math.pi*math.random(), 15, 12)

                if offset then
                    offset = offset + playerpos

                    local ent =

                    land_or_water == LAND and SpawnPrefab(
                        player.spawnlandshadow_fn ~= nil and player.spawnlandshadow_fn(player, params, ...) or
                        player.components.sanity:GetPercent() < .1 and
                        math.random() < TUNING.TERRORBEAK_SPAWN_CHANCE and
                        "terrorbeak" or
                        "crawlinghorror"
                    ) or
                    land_or_water == WATER and SpawnPrefab(
                        player.spawnseashadow_fn ~= nil and player.spawnseashadow_fn(player, params, ...) or
                        "swimminghorror"
                    )

                    if ent ~= nil then
                        ent.Transform:SetPosition(offset:Get())
                        StartTracking(player, params, ent)
                    end
                end
                --Reschedule spawning if we haven't reached our target population
                params.spawntask =
                    params.targetpop ~= #params.ents
                    and player:DoTaskInTime(TUNING.SANITYMONSTERS_SPAWN_INTERVAL + TUNING.SANITYMONSTERS_SPAWN_VARIANCE * math.random(), UpdateSpawn, params)
                    or nil
                return
            end
        end
        return _UpdateSpawn(player, params, ...)
    end

    UpvalueHacker.SetUpvalue(StartSpawn, UpdateSpawn, "UpdateSpawn")
end)
