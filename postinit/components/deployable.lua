local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------------------------------------------------------------------
local Deployable = require("components/deployable")

function Deployable:DeployAtRange()
    return self.deployatrange
end

function Deployable:GetDeployDist()
    return self.deploydistance
end

local _CanDeploy = Deployable.CanDeploy
function Deployable:CanDeploy(pt, mouseover, deployer, ...)
	local result = _CanDeploy(self, pt, mouseover, deployer, ...)

	if result then
		local tile = TheWorld.Map:GetTileAtPoint(pt:Get())
		local tileinfo = GetTileInfo(tile)
        local isbuildable, island, iswater = tileinfo and tileinfo.buildable or false, tileinfo and tileinfo.land ~= false or false, tileinfo and tileinfo.water or false

        if (self.candeployonland and island) or
        (self.candeployonshallowocean and tile == GROUND.OCEAN_SHALLOW) or
        (iswater and
        ((self.candeployonbuildableocean and isbuildable) or
        (self.candeployonunbuildableocean and not isbuildable))) then
			return result
		else
			return false
		end
	end

	return result
end

----------------------------------------------------------------------------------------
--Try to initialise all functions locally outside of the post-init so they exist in RAM only once
----------------------------------------------------------------------------------------

local function ondeployatrange(self, deployatrange)
    if self.inst.replica.inventoryitem ~= nil and self.inst.replica.inventoryitem.classified ~= nil then
        self.inst.replica.inventoryitem.classified.deployatrange:set(deployatrange)
    end
end

local function oncandeployonland(self, candeployonland)
    if self.inst.replica.inventoryitem ~= nil and self.inst.replica.inventoryitem.classified ~= nil then
        self.inst.replica.inventoryitem.classified.candeployonland:set(candeployonland)
    end
end

local function oncandeployonshallowocean(self, candeployonshallowocean)
    if self.inst.replica.inventoryitem ~= nil and self.inst.replica.inventoryitem.classified ~= nil then
        self.inst.replica.inventoryitem.classified.candeployonshallowocean:set(candeployonshallowocean)
    end
end

local function oncandeployonbuildableocean(self, candeployonbuildableocean)
    if self.inst.replica.inventoryitem ~= nil and self.inst.replica.inventoryitem.classified ~= nil then
        self.inst.replica.inventoryitem.classified.candeployonbuildableocean:set(candeployonbuildableocean)
    end
end

local function oncandeployonunbuildableocean(self, candeployonunbuildableocean)
    if self.inst.replica.inventoryitem ~= nil and self.inst.replica.inventoryitem.classified ~= nil then
        self.inst.replica.inventoryitem.classified.candeployonunbuildableocean:set(candeployonunbuildableocean)
    end
end

local function ondeploydistance(self, deploydistance)
    if self.inst.replica.inventoryitem ~= nil and self.inst.replica.inventoryitem.classified ~= nil then
        self.inst.replica.inventoryitem.classified.deploydistance:set(deploydistance)
    end
end

IAENV.AddComponentPostInit("deployable", function(cmp)
    addsetter(cmp, "deployatrange", ondeployatrange)
    addsetter(cmp, "candeployonland", oncandeployonland)
    addsetter(cmp, "candeployonshallowocean", oncandeployonshallowocean)
    addsetter(cmp, "candeployonbuildableocean", oncandeployonbuildableocean)
    addsetter(cmp, "candeployonunbuildableocean", oncandeployonunbuildableocean)
    addsetter(cmp, "deploydistance", ondeploydistance)

    cmp.deployatrange = false
    cmp.candeployonland = true
    cmp.candeployonshallowocean = false
    cmp.candeployonbuildableocean = false
    cmp.candeployonunbuildableocean = false
    cmp.deploydistance = 0
end)