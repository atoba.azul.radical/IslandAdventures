local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------
--Try to initialise all functions locally outside of the post-init so they exist in RAM only once
----------------------------------------------------------------------------------------

local function fn(inst)
    if TheWorld.ismastersim then
        local _COMBAT_CANTHAVE_TAGS = UpvalueHacker.GetUpvalue(inst.BecomeAggressive, "AggressiveRetarget", "COMBAT_CANTHAVE_TAGS")
        table.insert(_COMBAT_CANTHAVE_TAGS, "abi_notarget")
    end
end

IAENV.AddPrefabPostInit("abigail", fn)
