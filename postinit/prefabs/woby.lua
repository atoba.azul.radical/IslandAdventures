local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

local function WobyWaterfn(inst)
    inst:AddTag("amphibious") --makes woby walk on water, we need to add a swimming state with swimming animations
end
IAENV.AddPrefabPostInit("wobysmall", WobyWaterfn)
IAENV.AddPrefabPostInit("wobybig", WobyWaterfn)
