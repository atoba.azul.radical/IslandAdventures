local function SpawnMesssagebottle(inst, data)
	if not string.find(data.name, "ia_messagebottle") then
		return
	end
	local x, y
	local map = GLOBAL.TheWorld.Map
	local sx, sy = map:GetSize()
	local i = 1
	repeat
		x = math.random(-sx,sx)
		y = math.random(-sy,sy)
		local tile = map:GetTileAtPoint(x, 0, y)
		i = i + 1
	until  GLOBAL.IsWaterTile(tile) or i == 1000
	GLOBAL.SpawnPrefab("ia_messagebottle").Transform:SetPosition(x, 0, y)
end

local function installIAcomponents(inst)
	print("Loading world with IA:",inst:HasTag("forest") and "Has Forest" or "No Forest",inst:HasTag("island") and "Has Islands" or "No Islands")
	if inst.ismastersim then
		if inst:HasTag("island") then
			--inst:AddComponent('tiled')
			inst:AddComponent("worldislandtemperature")
			inst:AddComponent("volcanomanager")
			if not inst.components.lureplantspawner then
				inst:AddComponent("lureplantspawner")
			end
			if not inst:HasTag("volcano") then
				inst:AddComponent("hailrain")
				inst:AddComponent("wavemanager_ia") -- this excludes visuals, those are clientside only
				inst:AddComponent("chessnavy")
				inst:AddComponent("whalehunter")
				inst:AddComponent("tigersharker")
				inst:AddComponent("twisterspawner")
				inst:AddComponent("floodmosquitospawner")
				inst:AddComponent("rainbowjellymigration")
				inst:AddComponent("krakener")
				inst:AddComponent("timer") -- for ia_messagebottle respawn
				inst:ListenForEvent("timerdone", SpawnMesssagebottle)
			else
				inst:RemoveComponent("hounded")
			end
			--inst:AddSpoofedComponent("worldshorecollisions", "shorecollisions")
		end
		inst:AddComponent("doydoyspawner")
	end
	if inst:HasTag("island") and not inst:HasTag("volcano") then
		inst:AddComponent("flooding")
        inst.Flooding = {GetTileCenterPoint = function(self, x, y, z) return inst.components.flooding:GetTileCenterPoint(x, y, z) end} --GEOPLACEMENT SUPPORT
		GLOBAL.TileState_GroundCreep = true
		if inst.net and inst.net.components.weather then
			inst.net.components.weather.cannotsnow = true
		end
	end
	inst.installIAcomponents = nil --self-destruct after use
end

--------------------------------------------------------------------------

AddPrefabPostInit("world", function(inst)

--------------------------------------------------------------------------

inst.installIAcomponents = installIAcomponents

local OnPreLoad_old = inst.OnPreLoad
inst.OnPreLoad = function(...)
	local primaryworldtype = inst.topology and inst.topology.overrides and inst.topology.overrides.primaryworldtype
	local isvlcano = inst.topology and inst.topology.overrides and inst.topology.overrides.isvolcano
	if not inst.topology or not inst.topology.ia_worldgen_version then primaryworldtype = "default" end --pre-RoT fix
	if primaryworldtype == nil then primaryworldtype = "default" end --RoT: Forgotten Knowledge pruned world settings for a while.

	if isvlcano then
		inst:AddTag("volcano")
	end

	if primaryworldtype then
		if primaryworldtype ~= "default" and inst:HasTag("forest") then --crude caves fix
			inst:AddTag("island")
		end
		if primaryworldtype ~= "default" and primaryworldtype ~= "merged" then
			inst:RemoveTag("forest")
		end
	end

	if inst.installIAcomponents then
		inst:installIAcomponents()
	end

	return OnPreLoad_old and OnPreLoad_old(...)
end

--------------------------------------------------------------------------

end)
